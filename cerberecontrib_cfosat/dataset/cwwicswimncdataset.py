# -*- coding: utf-8 -*-
"""
Dataset class for CNES/CWWIC CFOSAT SWIM files
"""

from cerbere.dataset.ncdataset import NCDataset


class CWWICSWIMNCDataset(NCDataset):

    def __init__(self, *args, **kwargs):
        super(CWWICSWIMNCDataset, self).__init__(
            *args, decode_times=False, **kwargs
        )
