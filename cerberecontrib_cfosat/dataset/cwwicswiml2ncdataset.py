# -*- coding: utf-8 -*-
"""
Dataset class for CNES/CWWIC CFOSAT SWIM L2 files
"""
from collections import OrderedDict
import re
from typing import MutableMapping

import numpy as np
import xarray as xr

import cerbere.dataset as internals
from cerberecontrib_cfosat.dataset.cwwicswimncdataset import CWWICSWIMNCDataset


class CWWICSWIML2NCDataset(CWWICSWIMNCDataset):
    """Abstract class for CWWIC SWIM L2 files"""

    def _open(self, **kwargs):
        super(CWWICSWIML2NCDataset, self)._open(**kwargs)

        # fix and reshape time
        # translate = {
        #     self._native2std_field[k]: k for k in self._native2std_field
        # }
        # time_field = translate['time']
        time_field = 'time'
        if 'n_time' not in self._std_dataset[time_field].dims:
            timeref = np.datetime64('2009-01-01')
            times = timeref + self._std_dataset[time_field].astype(
                'timedelta64[s]')

        else:
            timeref = np.datetime64(re.search(
                r'(\d+\-\d+\-\d+ \d+\:\d+\:\d+)',
                self._std_dataset[time_field].units
            ).group(1))
            times = (
                timeref +
                self._std_dataset[time_field].isel(n_tim=0).astype('timedelta64[s]') +
                self._std_dataset[time_field].isel(n_tim=1).astype('timedelta64[us]')
            )

        self._std_dataset.coords['time'] = xr.DataArray(
            name='time',
            data=times,
            dims=self._std_dataset['time'].dims,
            attrs={'long_name': self._std_dataset['time'].attrs['long_name']})

        self._std_dataset.coords['time'].encoding[internals.ENCODING] = {}

        # fix time units
        # for v in ['time_spec_l2', 'time_nadir_l2',
        #           'stop_time_l2', 'start_time_l2']:
        #     units = self._std_dataset[v].encoding.get(
        #         'units', self._std_dataset[v].attrs.get('units'))
        #     try:
        #         self._std_dataset[v].encoding['units'] = units.replace(
        #             's+us', 'microseconds')
        #         if 'units' in self._std_dataset[v].attrs:
        #             self._std_dataset[v].attrs.pop('units')
        #     except ValueError:
        #         pass

        # assign coordinates
        self._std_dataset = self._std_dataset.set_coords(
          #  [translate['lat'], translate['lon'], 'time']
            ['lat', 'lon', 'time']
        )

    def get_collection_id(self):
        """return the identifier of the product collection"""
        return self.url.name[:21]

    @property
    def geodims(self) -> MutableMapping[str, int]:
        """Mapping from geolocation dimension names to lengths.
        """
        return OrderedDict([
            (_, self.dims[_]) for _ in self._std_dataset['time'].dims
            ])


class CWWICSWIML2BoxNCDataset(CWWICSWIML2NCDataset):
    """
    CWWIC L2 dataset as a swath/box product
    """
    def __init__(self, *args, **kwargs):
        field_matching = {
            'time_l2': 'time',
            'lon_l2': 'lon',
            'lat_l2': 'lat',
        }
        dim_matching = {
            'n_box': 'row',
            'n_posneg': 'cell',
        }
        super(CWWICSWIML2BoxNCDataset, self).__init__(
            *args,
            field_matching=field_matching,
            dim_matching=dim_matching,
            **kwargs
        )

    def _open(self, **kwargs):
        super(CWWICSWIML2BoxNCDataset, self)._open(**kwargs)

        # drop variables in other coordinate system (managed through other
        # subclasses
        dropvars = []
        for k, v in self._std_dataset.items():
            if 'n_mcycles' in v.dims:
                dropvars.append(k)
            if (k.startswith('nadir_') and
                    not set(v.dims).difference(set(['box', 'n_tim']))):
                dropvars.append(k)

        self._std_dataset = self._std_dataset.drop(dropvars)

        print(self.geodims)


class CWWICSWIML2NadirNCDataset(CWWICSWIML2NCDataset):
    """
    CWWIC L2 dataset as a nadir/trajectory product
    """
    def __init__(self, *args, **kwargs):
        field_matching = {
            'time_nadir_l2': 'time',
            'lon_nadir_l2': 'lon',
            'lat_nadir_l2': 'lat',
        }
        dim_matching = {
            'n_box': 'time',
        }
        super(CWWICSWIML2NadirNCDataset, self).__init__(
            *args,
            field_matching=field_matching,
            dim_matching=dim_matching,
            **kwargs
        )

    def _open(self, **kwargs):
        super(CWWICSWIML2NadirNCDataset, self)._open(**kwargs)

        # drop variables in other coordinate system (managed through other
        # subclasses
        dropvars = []
        for k, v in self._std_dataset.items():
            if 'n_mcycles' in v.dims:
                dropvars.append(k)
        self._std_dataset = self._std_dataset.drop(dropvars)


class CWWICSWIML2ANADNCDataset(CWWICSWIML2NCDataset):
    """
    CWWIC L2 ANAD dataset as a full resolution (5 Hz) nadir/trajectory product
    """
    def __init__(self, *args, **kwargs):
        field_matching = {
            'time_nadir_native': 'time',
            'lon_l2anad_0': 'lon',
            'lat_l2anad_0': 'lat',
        }
        dim_matching = {
            'n_mcycles': 'time',
        }
        super(CWWICSWIML2ANADNCDataset, self).__init__(
            *args,
            field_matching=field_matching,
            dim_matching=dim_matching,
            **kwargs
        )


class CWWICSWIML2ANAD1HzNCDataset(CWWICSWIML2NCDataset):
    """
    CWWIC L2ANAD dataset as a 1 Hz product
    """
    def __init__(self, *args, **kwargs):
        field_matching = {
            'time_nadir_1Hz': 'time',
            'lon_nadir_1Hz': 'lon',
            'lat_nadir_1Hz': 'lat',
        }
        dim_matching = {
            'n_nad_1Hz': 'time',
        }
        super(CWWICSWIML2ANAD1HzNCDataset, self).__init__(
            *args,
            field_matching=field_matching,
            dim_matching=dim_matching,
            **kwargs
        )
