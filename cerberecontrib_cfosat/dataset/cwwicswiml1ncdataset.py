# -*- coding: utf-8 -*-
"""
Dataset class for CNES/CWWIC CFOSAT SWIM L1A/L1B files
"""
from abc import abstractproperty
import re
from collections import OrderedDict
from dateutil import parser
import numpy as np
from ast import literal_eval
import logging
from typing import List

import xarray as xr

from cerberecontrib_cfosat.dataset.cwwicswimncdataset import CWWICSWIMNCDataset


# TODO:
# - read_field_attributes() : change long name when action=merge_var (beam->incidence)
# - not implemented functions to check (end of file)
# (get_bbox() : only for incidence ?)
# - add docstrings


def get_macrocycle_angles(url):
    """
    """
    dataset = xr.open_dataset(url, decode_times=False)
    if 'macrocycle_angle' in dataset.attrs:
        angles = dataset.macrocycle_angle
        if isinstance(angles, np.ndarray):
            angles = angles.tolist()
        elif isinstance(angles, str) and ';' in angles:
            angles = angles.replace(';', ',')
            angles = literal_eval('[{}]'.format(angles))
        else:
            dataset.close()
            raise Exception(f'Unexpected macrocycle_angle attribute : {angles}')
    elif len(dataset.dimensions.get('n_beam', [])) == 6:
        angles = [0, 2, 4, 6, 8, 10]
    else:
        dataset.close()
        raise Exception('Could not find macrocycle angles.')
    dataset.close()

    return angles


class IncidenceError(Exception):
    pass


def time_units_factor(unit1, unit2):
    """
    """
    factors = {'seconds': {'seconds': 1.,
                           'milliseconds': 1000.,
                           'microseconds': 1000000.},
               'milliseconds': {'seconds': 0.001,
                                'milliseconds': 1.,
                                'microseconds': 1000.},
               'microseconds': {'seconds': 0.000001,
                                'milliseconds': 0.001,
                                'microseconds': 1.}}
    if unit1 not in factors or unit2 not in factors[unit1]:
        raise Exception('Which factor for {} to {} ?'.format(unit1, unit2))
    return factors[unit1][unit2]


class CWWICSWIML1NCDataset(CWWICSWIMNCDataset):
    """Abstract class for CWWIC SWIM L1 files

    The dataset class virtually modifies the format in order to give dimensions,
    variables and attributes for a given incidence angle instead of a given beam
     (which is different for non nominal macrocycles).

    To be used with a Trajectory feature class.
    """
    def __init__(
            self, *args, incidence=0, mode='r', aux_mcycle=None,  **kwargs):
        """
        """
        self._vars2attr = OrderedDict()
        self._incidence = incidence
        self._l1type = None
        self._mcycle = OrderedDict()
        self._vars = OrderedDict()
        self._dims = OrderedDict()
        self._aux_mcycle = aux_mcycle

        if mode != 'r':
            raise Exception('Only mode {} is accepted.'.format('r'))
        super(CWWICSWIML1NCDataset, self).__init__(*args, **kwargs)

    @property
    def macrocycle(self) -> np.ndarray:
        """Get macrocycle"""
        try:
            mc_angle = self._std_dataset.attrs['macrocycle_angle']
        except ValueError:
            mc_angle = None

        if isinstance(mc_angle, np.ndarray):
            pass
        elif isinstance(mc_angle, str) and ';' in mc_angle:
            mc_angle = mc_angle.replace(';', ',')
            mc_angle = np.array(literal_eval('[{}]'.format(mc_angle)))
        else:
            raise Exception('Unexpected macrocycle_angle attribute.')

        return mc_angle

    @abstractproperty
    def n_beam(self):
        raise NotImplementedError

    @abstractproperty
    def _variables_per_beam(self):
        raise NotImplementedError

    @abstractproperty
    def _dims_per_beam(self):
        raise NotImplementedError

    def _reduce_by_beam(self):
        """keep only the variables and dimensions related to the selected
        incidence.
        """
        self._std_dataset = self._std_dataset.sel(n_beam=self._incidence)

        # remove variables for other beams than the selected one
        renamed_vars = {}
        dropped_vars = []
        for v in self._variables_per_beam:
            for i_beam in range(6):
                vbeam = f'{v}_{i_beam}'
                if i_beam == self._incidence:
                    renamed_vars[vbeam] = v
                else:
                    dropped_vars.append(vbeam)
        self._std_dataset = self._std_dataset.drop(dropped_vars).rename_vars(
            renamed_vars
        )

        # remove dims for other beams than the selected one
        renamed_dims = {}
        dropped_dims = []
        for d in self._dims_per_beam:
            for i_beam in range(6):
                dbeam = f'{d}_{i_beam}'
                if i_beam == self._incidence:
                    renamed_dims[dbeam] = d
                else:
                    print(dbeam, self._std_dataset.dims)
                    if dbeam in self._std_dataset.dims:
                        dropped_dims.append(dbeam)
        self._std_dataset = self._std_dataset.drop(dropped_dims).rename_dims(
            renamed_dims
        )
        print(self._std_dataset)

    def _open(self, **kwargs):
        super(CWWICSWIML1NCDataset, self)._open(**kwargs)

        mc_angle = self.macrocycle
        self._mcycle['mcycle'] = {'angle': mc_angle}

        # Parse dimensions in order to find n_beam* dims
        nat_dims = self.dims
        beam_dnames = []
        for nat_dname in nat_dims.keys():
            if re.match(r'^n_beam.*$', str(nat_dname)) is None:
                continue
            if nat_dname == 'n_beam':
                b_angle = mc_angle.copy()
            elif nat_dname == 'n_beamM1':
                b_angle = mc_angle[np.where(mc_angle > 0)]
            elif nat_dname == 'n_beam_l1b':
                b_angle = mc_angle[np.where(mc_angle >= 6)]
            elif nat_dname == 'n_beam0':
                b_angle = mc_angle[np.where(mc_angle == 0)]
            else:
                raise Exception(f'{nat_dname} is a new n_beam* dim ?')
            beam_dnames.append(nat_dname)
            self._mcycle[nat_dname] = {'angle': b_angle}

        # Get index/count for mcycle and n_beam*
        for d in self._mcycle.keys():
            index = np.where(
                self._mcycle[d]['angle'] == self._incidence)[0].tolist()
            if len(index) != 0:
                if any([i + index[0] != index[i] for i in range(len(index))]):
                    raise Exception(
                        f'Not adjacent beams for incidence={self._incidence} ?')
            self._mcycle[d]['index'] = index
            self._mcycle[d]['count'] = len(index)

        if self.n_beam == 0:
            raise IncidenceError(
                f'incidence {self._incidence} not found in {self.url}')

        # Parse variables
        nat_vars = self._std_dataset.variables
        for nat_vname in nat_vars.keys():
            nat_dnames = self._std_dataset[nat_vname].dims
            nat_dvals = [nat_dims[nat_dname] for nat_dname in nat_dnames]
            is_numbered = re.match(r'^.*_[0-5]$', nat_vname) is not None
            has_nmcycles = 'n_mcycles' in nat_dnames
            beam_dname = [d for d in beam_dnames if d in nat_dnames]
            if len(beam_dname) > 1:
                txt = '{} with more than one n_beam* dim ?'
                raise Exception(txt.format(nat_vname))
            has_beam = len(beam_dname) == 1
            if nat_vname == 'echo_l1_0':
                # special case : n_mcycles & n_beam & n_swath
                vname = 'echo_l1'
                if not has_beam:
                    raise Exception
                beam_dname = beam_dname[0]
                if self._mcycle[beam_dname]['count'] == 0:
                    continue
                dnames = []
                dvals = []
                for nat_dname, nat_dval in zip(nat_dnames, nat_dvals):
                    if nat_dname == 'n_mcycles':
                        dnames.append('time')
                        dvals.append(nat_dval * self._mcycle[beam_dname]['count'])
                    elif nat_dname == beam_dname:
                        pass
                    elif nat_dname == 'n_swath_0_cor':
                        dnames.append('n_swath_cor')
                        dvals.append(nat_dval)
                    else:
                        raise Exception
                self._vars[vname] = {'nat_vnames': [nat_vname],
                                     'nat_dnames': [nat_dnames],
                                     'nat_dvals': [nat_dvals],
                                     'dnames': dnames,
                                     'dvals': dvals,
                                     'beam_dname': beam_dname,
                                     'action': 'merge_dim'}
            elif is_numbered:
                #if not has_nmcycles:
                    #txt = '{} without n_mcycles dim ?'
                    #raise Exception(txt.format(nat_vname))
                if has_beam:
                    raise Exception('{nat_vname} with n_beam* dim ?')

                split_nat_vname = nat_vname.split('_')
                vname = '_'.join(split_nat_vname[0:-1])
                vbeam = split_nat_vname[-1]
                if int(vbeam) >= self._mcycle['mcycle']['index'][0] and \
                   int(vbeam) <= self._mcycle['mcycle']['index'][-1]:
                    dnames = []
                    dvals = []
                    for nat_dname, nat_dval in zip(nat_dnames, nat_dvals):
                        if nat_dname == 'n_mcycles':
                            dnames.append('time')
                        elif re.match(r'^.*_[0-5]$', nat_dname) is not None:
                            split_nat_dname = nat_dname.split('_')
                            dname = '_'.join(split_nat_dname[0:-1])
                            dbeam = split_nat_dname[-1]
                            if vbeam != dbeam:
                                txt = '{} with {} dim ?'
                                raise Exception(txt.format(nat_vname, nat_dname))
                            dnames.append(dname)
                        else:
                            dnames.append(nat_dname)
                        dvals.append(nat_dval)
                    if not has_nmcycles:
                        # eg. reliable_swath_[0-5] (L1A>=4.3.0)
                        # With a non nominal macrocycle, (0,8,8) for instance,
                        # we assume reliable_swath_1=reliable_swath_2
                        # and read only one of the variables
                        if vname not in self._vars.keys():
                            self._vars[vname] = {'nat_vnames': [nat_vname],
                                                 'nat_dnames': [nat_dnames],
                                                 'nat_dvals': [nat_dvals],
                                                 'dnames': dnames,
                                                 'dvals': dvals,
                                                 'action': 'none'}
                    elif vname not in self._vars.keys():
                        self._vars[vname] = {'nat_vnames': [nat_vname],
                                             'nat_dnames': [nat_dnames],
                                             'nat_dvals': [nat_dvals],
                                             'dnames': dnames,
                                             'dvals': dvals,
                                             'action': 'merge_var'}
                    else:
                        chk_dnames = self._vars[vname]['dnames']
                        if len(dnames) != len(chk_dnames) or \
                           any([d1 != d2 for d1, d2 in zip(dnames, chk_dnames)]):
                            txt = '{} not consistent with {}_*'
                            raise Exception(txt.format(nat_vname, vname))
                        chk_dvals = self._vars[vname]['nat_dvals'][-1]
                        if len(dvals) != len(chk_dvals) or \
                           any([d1 != d2 for d1, d2 in zip(dvals, chk_dvals)]):
                            txt = '{} not consistent with {}_*'
                            raise Exception(txt.format(nat_vname, vname))
                        tmp_nat_vnames = list(self._vars[vname]['nat_vnames'])
                        tmp_nat_vnames.append(nat_vname)
                        tmp_nat_vnames.sort()
                        vindex = tmp_nat_vnames.index(nat_vname)
                        self._vars[vname]['nat_vnames'].insert(vindex, nat_vname)
                        self._vars[vname]['nat_dnames'].insert(vindex, nat_dnames)
                        self._vars[vname]['nat_dvals'].insert(vindex, nat_dvals)
                        time_pos = dnames.index('time')
                        self._vars[vname]['dvals'][time_pos] += dvals[time_pos]
            elif has_nmcycles:
                if not has_beam:
                    count = self._mcycle['mcycle']['count']
                    if count == 0:
                        continue
                    dnames, dvals = [], []
                    for nat_dname, nat_dval in zip(nat_dnames, nat_dvals):
                        if nat_dname == 'n_mcycles':
                            dnames.append('time')
                            dvals.append(nat_dval * count)
                        else:
                            dnames.append(nat_dname)
                            dvals.append(nat_dval)
                    if count == 1:
                        self._vars[nat_vname] = {'nat_vnames': [nat_vname],
                                                 'nat_dnames': [nat_dnames],
                                                 'nat_dvals': [nat_dvals],
                                                 'dnames': dnames,
                                                 'dvals': dvals,
                                                 'action': 'none'}
                    else:
                        self._vars[nat_vname] = {'nat_vnames': [nat_vname],
                                                 'nat_dnames': [nat_dnames],
                                                 'nat_dvals': [nat_dvals],
                                                 'dnames': dnames,
                                                 'dvals': dvals,
                                                 'action': 'duplicate_nmcycles'}
                else:
                    beam_dname = beam_dname[0]
                    if self._mcycle[beam_dname]['count'] == 0:
                        continue
                    dnames = []
                    dvals = []
                    for nat_dname, nat_dval in zip(nat_dnames, nat_dvals):
                        if nat_dname == 'n_mcycles':
                            dnames.append('time')
                            dvals.append(nat_dval * self._mcycle[beam_dname]['count'])
                        elif nat_dname == beam_dname:
                            pass
                        else:
                            dnames.append(nat_dname)
                            dvals.append(nat_dval)
                    self._vars[nat_vname] = {'nat_vnames': [nat_vname],
                                             'nat_dnames': [nat_dnames],
                                             'nat_dvals': [nat_dvals],
                                             'dnames': dnames,
                                             'dvals': dvals,
                                             'beam_dname': beam_dname,
                                             'action': 'merge_dim'}
            elif has_beam: # no n_mcycles dim here
                beam_dname = beam_dname[0]
                if self._mcycle[beam_dname]['count'] == 0:
                    continue
                if len(nat_dnames) != 1: # n_beam* dim will be removed
                    dnames = []
                    dvals = []
                    for nat_dname, nat_dval in zip(nat_dnames, nat_dvals):
                        if nat_dname != beam_dname:
                            dnames.append(nat_dname)
                            dvals.append(nat_dval)
                    self._vars[nat_vname] = {'nat_vnames': [nat_vname],
                                             'nat_dnames': [nat_dnames],
                                             'nat_dvals': [nat_dvals],
                                             'dnames': dnames,
                                             'dvals': dvals,
                                             'beam_dname': beam_dname,
                                             'action': 'remove_nbeam'}
                else: # will become attribute
                    self._vars2attr[nat_vname] = {'nat_vnames': [nat_vname],
                                                  'nat_dnames': [nat_dnames],
                                                  'nat_dvals': [nat_dvals],
                                                  'beam_dname': beam_dname}
            else:
                self._vars[nat_vname] = {'nat_vnames': [nat_vname],
                                         'nat_dnames': [nat_dnames],
                                         'nat_dvals': [nat_dvals],
                                         'dnames': nat_dnames,
                                         'dvals': nat_dvals,
                                         'action': 'none'}
        for vname in self._vars.keys():
            for dname, dval in zip(self._vars[vname]['dnames'], \
                                   self._vars[vname]['dvals']):
                if dname not in self._dims:
                    self._dims[dname] = dval
                elif self._dims[dname] != dval:
                    txt = 'Inconsistent dimension in {}'
                    raise Exception(txt.format(vname))

        # keep only one incidence
        self._reduce_by_beam()

        print("VARS ", list(self._std_dataset.variables.keys()))

    def read_values(self, fieldname, slices=None):
        """
        """
        assert isinstance(slices, (type(None), list, tuple))
        if fieldname in self._vars.keys():
            _var = self._vars[fieldname]
            action = _var['action']
            if action == 'none':
                nat_vname = _var['nat_vnames'][0]
                values = super(CWWICSWIML1NCDataset, self).read_values(nat_vname, slices)
            elif action in ['merge_dim', 'merge_var', 'duplicate_nmcycles']:
                # Make filled slices for fieldname
                dvals = _var['dvals']
                if slices is None:
                    tmp_slices = [slice(None)] * len(dvals)
                else:
                    tmp_slices = list(slices)
                filled_slices = super(CWWICSWIML1NCDataset, self)._fill_slices(tmp_slices, dvals)
                # Adapt slices and read native fieldname(s)
                # 'merge_dim' : incidence is split in a n_beam* dimension
                # 'merge_var' : incidence is split between *_{beam} variables
                # For both cases, the "beam" dimension is put after n_mcycles dimension
                # in order to facilitate the merge (simple reshape)
                nat_slices = list(filled_slices)
                nat_vnames = _var['nat_vnames']
                nat_dnames = _var['nat_dnames']
                if action == 'merge_dim':
                    # add a slice for n_beam* dimensions
                    beam_dname = _var['beam_dname']
                    beam_dpos = nat_dnames[0].index(beam_dname)
                    index = self._mcycle[beam_dname]['index']
                    nat_slices.insert(beam_dpos, slice(index[0], index[-1] + 1, 1))
                    # cancel n_mcycles slice (is input time slice)
                    nmcycle_dpos = nat_dnames[0].index('n_mcycles')
                    nat_slices[nmcycle_dpos] = slice(None)
                    # read variable and put n_beam* dim after n_mcycles dim
                    values = super(CWWICSWIML1NCDataset, self).read_values(nat_vnames[0], nat_slices)
                    if beam_dpos > nmcycle_dpos:
                        values = np.rollaxis(values, beam_dpos, nmcycle_dpos + 1)
                    else:
                        values = np.rollaxis(values, nmcycle_dpos, beam_dpos)
                        nmcycle_dpos = beam_dpos
                elif action == 'merge_var':
                    # cancel n_mcycles slice (is input time slice)
                    nmcycle_dpos = nat_dnames[0].index('n_mcycles')
                    nat_slices[nmcycle_dpos] = slice(None)
                    # read variables, create a virtual "beam" dim and
                    # put it after n_mcycles dim
                    beam_values = []
                    for nat_vname in nat_vnames:
                        b_values = super(CWWICSWIML1NCDataset, self).read_values(nat_vname, nat_slices)
                        beam_values.append(np.ma.expand_dims(b_values, -1))
                    values = np.ma.concatenate(beam_values, axis=-1)
                    values = np.rollaxis(values, -1, nmcycle_dpos + 1)
                elif action == 'duplicate_nmcycles':
                    # cancel n_mcycles slice (is input time slice)
                    nmcycle_dpos = nat_dnames[0].index('n_mcycles')
                    nat_slices[nmcycle_dpos] = slice(None)
                    # read variable, duplicate it in a virtual "beam" dim and
                    # put it after n_mcycles dim
                    values = super(CWWICSWIML1NCDataset, self).read_values(nat_vnames[0], nat_slices)
                    beam_values = []
                    beam_count = self._mcycle['mcycle']['count']
                    for _ in range(beam_count):
                        beam_values.append(np.ma.expand_dims(values, -1))
                    values = np.ma.concatenate(beam_values, axis=-1)
                    values = np.rollaxis(values, -1, nmcycle_dpos + 1)
                # Merge n_mcycles / "beam" dims into a time dim
                old_shape = values.shape
                new_shape = []
                for i in range(len(old_shape)):
                    if i == nmcycle_dpos:
                        new_shape.append(old_shape[i] * old_shape[i + 1])
                    elif i == nmcycle_dpos + 1:
                        pass
                    else:
                        new_shape.append(old_shape[i])
                values = values.reshape(new_shape, order='C')
                # Apply time slice
                if slices is not None:
                    time_slices = [slice(None)] * len(dvals)
                    dnames = _var['dnames']
                    time_dpos = dnames.index('time')
                    time_slices[time_dpos] = filled_slices[time_dpos]
                    values = values[tuple(time_slices)]
                # Check all dimensions (to be removed in future)
                shape = values.shape
                if len(shape) != len(filled_slices):
                    txt = '{} with unexpected dims after merge.'
                    raise Exception(txt.format(fieldname))
                for shp, sli in zip(shape, filled_slices):
                    start, stop, step = sli.start, sli.stop, sli.step
                    if stop is None:
                        stop = -1
                    dimsize = 1 + (abs(stop - start) - 1) / abs(step)
                    if shp != dimsize:
                        txt = '{} with unexpected dims after merge.'
                        raise Exception(txt.format(fieldname))
            elif action == 'remove_nbeam':
                # Make filled slices for fieldname
                dvals = _var['dvals']
                if slices is None:
                    tmp_slices = [slice(None)] * len(dvals)
                else:
                    tmp_slices = list(slices)
                filled_slices = super(CWWICSWIML1NCDataset, self)._fill_slices(tmp_slices, dvals)
                # Adapt slices and read native fieldname
                # A 1-length slice is added for n_beam* dim, no matter the macrocycle
                nat_slices = list(filled_slices)
                nat_vnames = _var['nat_vnames']
                nat_dnames = _var['nat_dnames']
                beam_dname = _var['beam_dname']
                beam_dpos = nat_dnames[0].index(beam_dname)
                index = self._mcycle[beam_dname]['index']
                nat_slices.insert(beam_dpos, slice(index[0], index[0] + 1, 1))
                values = super(CWWICSWIML1NCDataset, self).read_values(nat_vnames[0], nat_slices)
                # Remove the n_beam* dim
                values = np.ma.squeeze(values, axis=beam_dpos)
            else:
                raise Exception('Unknown action for {}'.format(fieldname))
        elif fieldname in ['time', 'lon', 'lat']:
            if slices is None:
                ntime = self.get_dimsize('time')
                slices = [slice(ntime)]
            if fieldname == 'time' and self._l1type in ['L1A', 'L1B']:
                time_field = self.read_field('time')
                dtype = time_field.datatype
                units = time_field.units.split(' ')[0]
                # Read native time
                # (ref time should be the same -> no offsets)
                if self._l1type == 'L1A':
                    _fieldname = 'time_nr'
                elif self._l1type == 'L1B':
                    _fieldname = 'time_l1b'
                _units = self.read_field(_fieldname).units.split(' ')[0]
                if 'n_tim' in self.get_dimensions(_fieldname):
                    _units = _units.split('+')
                    if len(_units) != 2 or self.get_dimsize('n_tim') != 2:
                        raise Exception
                    _fac = np.zeros(2, dtype=dtype)
                    for i in range(2):
                        _fac[i] = time_units_factor(_units[i], units)
                    _slices = list(slices)
                    _slices.append(slice(0, 2))
                    _time = self.read_values(_fieldname, slices=_slices)
                    values = (_time[:, 0] * _fac[0] + \
                              _time[:, 1] * _fac[1]).astype(dtype)
                else:
                    _fac = np.array(time_units_factor(_units, units), dtype=dtype)
                    _time = self.read_values(_fieldname, slices=slices)
                    values = (_time * _fac).astype(dtype)
                # Read time_swath (old L1A case)
                if 'time_swath' in self.get_fieldnames():
                    swath_units = self.read_field('time_swath').units.split(' ')[0]
                    swath_fac = np.array(time_units_factor(swath_units, units), dtype=dtype)
                    nswath = self.get_dimsize('n_swath')
                    _slices = list(slices)
                    _slices.append(slice(nswath / 2, nswath / 2 + 1))
                    time_swath = self.read_values('time_swath', slices=_slices)
                    time_swath = time_swath.reshape(time_swath.shape[0])
                    values += (time_swath * swath_fac).astype(dtype)
            elif fieldname in ['lon', 'lat'] and self._l1type == 'L1A':
                nswath = self.get_dimsize('n_swath')
                _slices = list(slices)
                _slices.append(slice(nswath / 2, nswath / 2 + 1))
                _fieldname = '{}_l1a'.format(fieldname)
                values = self.read_values(_fieldname, slices=_slices)
                values = values.reshape(values.shape[0])
            elif fieldname in ['lon', 'lat'] and self._l1type == 'L1B':
                _fieldname = '{}_l1b'.format(fieldname)
                values = self.read_values(_fieldname, slices=slices)
            else:
                raise Exception('{} with {} ?'.format(fieldname, self._l1type))
        else:
            txt = 'Field {} not existing in mapper or NetCDF file.'
            raise Exception(txt.format(fieldname))
        return values

    # def read_field(self, fieldname):
    #     """
    #     """
    #     # Get NetCDF Variable
    #     naming_auth = self.get_naming_authority()
    #     attrs = self.read_field_attributes(fieldname)
    #     if 'long_name' in attrs:
    #         descr = attrs['long_name']
    #     else:
    #         descr = None
    #     if 'standard_name' in attrs:
    #         stdname = attrs['standard_name']
    #     else:
    #         stdname = None
    #     var = Variable(
    #         shortname=fieldname,
    #         description=descr,
    #         authority=naming_auth,
    #         standardname=stdname
    #         )
    #     dims = self.get_full_dimensions(fieldname)
    #     if 'scale_factor' in attrs:
    #         datatype = np.dtype(attrs['scale_factor'])
    #     elif 'add_offset' in attrs:
    #         datatype = np.dtype(attrs['add_offset'])
    #     else:
    #         if fieldname == 'time':
    #             datatype = np.dtype('float64')
    #         else:
    #             if fieldname in ['lon', 'lat']:
    #                 vname = fieldname + '_' + self._l1type.lower()
    #             else:
    #                 vname = fieldname
    #             _var = self._vars[vname]
    #             nat_vname = _var['nat_vnames'][0]
    #             datatype = self.get_handler().variables[nat_vname].dtype
    #     if '_FillValue' in attrs:
    #         fillvalue = attrs['_FillValue']
    #     elif 'fill_value' in attrs:
    #         fillvalue = attrs['fill_value']
    #     elif 'missing_value' in attrs:
    #         fillvalue = attrs['missing_value']
    #     else:
    #         fillvalue = None
    #     field = Field(var, dims, datatype=datatype, fillvalue=fillvalue)
    #     field.attach_storage(self.get_field_handler(fieldname))
    #     # MetaData
    #     field.attributes = {}
    #     field.units = None
    #     if 'units' in attrs:
    #         field.units = attrs['units']
    #     field.valid_min = None
    #     field.valid_max = None
    #     if 'valid_min' in attrs and 'valid_max' in attrs:
    #         # sometimes expressed as a string : need type cast
    #         try:
    #             field.valid_min = np.array(attrs['valid_min']).astype(
    #                 field.datatype)
    #             field.valid_max = np.array(attrs['valid_max']).astype(
    #                 field.datatype)
    #             if 'scale_factor' in attrs:
    #                 field.valid_min = field.valid_min * attrs['scale_factor']
    #                 field.valid_max = field.valid_max * attrs['scale_factor']
    #             if 'add_offset' in attrs:
    #                 field.valid_min = field.valid_min + attrs['add_offset']
    #                 field.valid_max = field.valid_max + attrs['add_offset']
    #         except:
    #             field.valid_min = attrs['valid_min']
    #             field.valid_max = attrs['valid_max']
    #             # logging.error("invalid valid_min or valid_max : %s %s",
    #             #               field.valid_min,
    #             #               field.valid_max)
    #     for att in attrs:
    #         if att not in ['units', 'scale_factor', 'add_offset',
    #                        '_FillValue', 'valid_min', 'valid_max']:
    #             field.attributes[att] = attrs[att]
    #     return field

    @property
    def fieldnames(self):
        """
        """
        return list(self._vars.keys())

    # def get_dimensions(self, fieldname=None):
    #     """
    #     """
    #     if fieldname is None:
    #         dims = self._dims.keys()
    #     elif fieldname in ['time', 'lon', 'lat']:
    #         dims = ['time']
    #     elif fieldname in self.get_fieldnames():
    #         dims = self._vars[fieldname]['dnames']
    #     else:
    #         txt = 'Field {} not existing in NetCDF file or mapper.'
    #         raise Exception(txt.format(fieldname))
    #     return tuple(dims)

    # def get_full_dimensions(self, fieldname=None):
    #     Parent's method should work

    # def get_dimsize(self, dimname):
    #     """
    #     """
    #     if dimname in self._dims:
    #         return self._dims[dimname]
    #     else:
    #         return super(SWIMCWWICL1NCFile, self).get_dimsize(dimname)

    def read_field_attributes(self, fieldname):
        """
        """
        if fieldname in self._vars.keys():
            _var = self._vars[fieldname]
            nat_vname = _var['nat_vnames'][0]
            attrs = super(SWIMCWWICL1NCFile, self).read_field_attributes(nat_vname)
            if fieldname == 'time_nr' or fieldname == 'time_l1b':
                units = attrs['units']
                if 'since' not in units:
                    if 'reference_time' in self.read_global_attributes():
                        ref_time = self.read_global_attribute('reference_time')
                    else: # for old L1B: assume same ref as old L1A
                        ref_time = '2009-01-01T00:00:00'
                    units += ' since ' + ref_time
                split_units = units.split(' ')
                if split_units[0] == 's':
                    units = 'seconds ' + ' '.join(split_units[1:])
                elif split_units[0] == 'us':
                    units = 'microseconds ' + ' '.join(split_units[1:])
                elif split_units[0] == 's+us':
                    units = 'seconds+microseconds ' + ' '.join(split_units[1:])
                else:
                    raise Exception('Which unit for {} ?'.format(fieldname))
                attrs['units'] = units
            elif fieldname == 'time_swath':
                units = attrs['units']
                if units == u'\xb5s':
                    units = 'microseconds'
                elif units == 'us':
                    units = 'microseconds'
                else:
                    raise Exception('Which unit for {} ?'.format(fieldname))
                attrs['units'] = units
        elif fieldname == 'time':
            if self._l1type == 'L1A':
                temp = self.read_field_attributes('time_nr')
                long_name = 'Swath middle gate time'
            elif self._l1type == 'L1B':
                temp = self.read_field_attributes('time_l1b')
                long_name = 'Footprint center time'
            attrs = OrderedDict()
            attrs['long_name'] = long_name
            attrs['_FillValue'] = temp['_FillValue']
            ref_time = temp['units'].split('since')[1].strip()
            ref_datetime = num2date(0, 'seconds since {}'.format(ref_time))
            ref_time = ref_datetime.strftime('%Y-%m-%dT%H:%M:%SZ')
            attrs['units'] = 'seconds since {}'.format(ref_time)
        elif fieldname == 'lon':
            if self._l1type == 'L1A':
                temp = self.read_field_attributes('lon_l1a')
                long_name = 'Swath middle gate longitude'
            elif self._l1type == 'L1B':
                temp = self.read_field_attributes('lon_l1b')
                long_name = 'Footprint center longitude'
            attrs = OrderedDict()
            attrs['long_name'] = long_name
            attrs['_FillValue'] = temp['_FillValue']
            attrs['units'] = temp['units']
        elif fieldname == 'lat':
            if self._l1type == 'L1A':
                temp = self.read_field_attributes('lat_l1a')
                long_name = 'Swath middle gate latitude'
            elif self._l1type == 'L1B':
                temp = self.read_field_attributes('lat_l1b')
                long_name = 'Footprint center latitude'
            attrs = OrderedDict()
            attrs['long_name'] = long_name
            attrs['_FillValue'] = temp['_FillValue']
            attrs['units'] = temp['units']
        else:
            txt = 'Field {} not existing in mapper or NetCDF file.'
            raise Exception(txt.format(fieldname))
        return attrs

    @property
    def attrs(self):
        """
        """
        #return self._vars2attr.update(
        #       super(CWWICSWIML1NCDataset, self).attrs()
        return super(CWWICSWIML1NCDataset, self).attrs

    def read_global_attribute(self, name):
        """
        """
        if name in self._vars2attr.keys():
            _var = self._vars2attr[name]
            nat_vname = _var['nat_vnames'][0]
            atts = super(SWIMCWWICL1NCFile, self).read_values(nat_vname)
            atts = atts[self._mcycle[_var['beam_dname']]['index']]
            if atts.min() != atts.max():
                txt = '{} with different values for one incidence ?'
                raise Exception(txt.format(name))
            att = atts[0]
        elif name in super(SWIMCWWICL1NCFile, self).read_global_attributes():
            att = super(SWIMCWWICL1NCFile, self).read_global_attribute(name)
            if name in ['reference_time', 'creation_date',
                        'start_time', 'stop_time',
                        'start_date', 'stop_date']:
                att = att.replace(';', '')
                att = att.replace('UTC=', '')
            elif isinstance(att, basestring) and ';' in att:
                try:
                    att = att.replace(';', ',')
                    att = np.array(literal_eval('[{}]'.format(att)))
                except:
                    pass
            if isinstance(att, np.ndarray):
                if name in ['macrocycle_angle', 'macrocycle_beam']:
                    pass
                else:
                    nvalues = att.size
                    nbeams = [v['angle'].size for v in self._mcycle.values()]
                    if nvalues in nbeams:
                        dim = self._mcycle.keys()[nbeams.index(nvalues)]
                        if self._mcycle[dim]['count'] == 0:
                            att = None
                        else:
                            att = att[self._mcycle[dim]['index']]
                            if att.min() != att.max():
                                txt = '{} with different values for one incidence ?'
                                if name in ['max_ranges_gate_index']:
                                    logging.warning(txt.format(name))
                                else:
                                    raise Exception(txt.format(name))
                            att = att[0]
                    elif nvalues == 6 and name in ['signal_sampling_in_radar_geometry',
                                                   'signal_resolution_in_radar_geometry']:
                        # Temporary for bad formatted L1.
                        # We have a non-nominal macrocycle and we assume attribute
                        # is given for a nominal macrocycle.
                        att = att[[0, 2, 4, 6, 8, 10].index(self._incidence)]
        else:
            txt = 'Attribute {} not existing in NetCDF file.'
            raise Exception(txt.format(name))
        return att

    @property
    def time_coverage_start(self) -> 'datetime.datetime':
        """
        """
        if 'time_coverage_start' in self.attrs:
            attrdate = self.dataset.attrs['time_coverage_start']
            attrdate = attrdate.rstrip('Z')
        else:
            start_date = self.dataset.attrs['start_date']
            start_time = self.dataset.attrs['start_time']
            attrdate = start_date + 'T' + start_time
        if attrdate.startswith('AAAA') and self._l1type == 'AUXMETEO':
            return None

        return parser.parse(attrdate)

    @property
    def time_coverage_end(self) -> 'datetime.datetime':
        """
        """
        if 'time_coverage_end' in self.read_global_attributes():
            attrdate = self.read_global_attribute('time_coverage_end')
            attrdate = attrdate.rstrip('Z')
        else:
            stop_date = self.read_global_attribute('stop_date')
            stop_time = self.read_global_attribute('stop_time')
            attrdate = stop_date + 'T' + stop_time
        if attrdate.startswith('AAAA') and self._l1type == 'AUXMETEO':
            return None

        return parser.parse(attrdate)


class CWWICSWIML1ANCDataset(CWWICSWIML1NCDataset):
    """Abstract class for CWWIC SWIM L1A files

    The dataset class virtually modifies the format in order to give dimensions,
    variables and attributes for a given incidence angle instead of a given beam
     (which is different for non nominal macrocycles).

    To be used with a Trajectory feature class.
    """
    @property
    def n_beam(self):
        print("CYCLE ", self._mcycle)
        return self._mcycle['n_beam']['count']

    @property
    def _variables_per_beam(self):
        return ['echo_l1a', 'lat_l1a', 'lon_l1a', 'radar_range', 'elevation',
                'incidence', 'ground_range', 'cal_ratio', 'reliable_swath']

    @property
    def _dims_per_beam(self):
        return ['n_swath']


class CWWICSWIML1BNCDataset(CWWICSWIML1NCDataset):
    """Abstract class for CWWIC SWIM L1B files

    The dataset class virtually modifies the format in order to give dimensions,
    variables and attributes for a given incidence angle instead of a given beam
     (which is different for non nominal macrocycles).

    To be used with a Trajectory feature class.
    """
    @property
    def n_beam(self):
        return self._mcycle['n_beam_l1b']['count']


class CWWICSWIML1AuxNCDataset(CWWICSWIML1NCDataset):
    """Abstract class for CWWIC SWIM L1A files

    The dataset class virtually modifies the format in order to give dimensions,
    variables and attributes for a given incidence angle instead of a given beam
    (which is different for non nominal macrocycles).

    To be used with a Trajectory feature class.
    """

    @property
    def macrocycle(self) -> np.ndarray:
        """Get macrocycle"""
        mc_angle = super(CWWICSWIML1AuxNCDataset, self).macrocycle
        if mc_angle is None:
            if self._aux_mcycle is not None:
                mc_angle = np.array(self._aux_mcycle)
            elif len(self._std_dataset.dims['n_beam']) == 6:
                mc_angle = np.array([0, 2, 4, 6, 8, 10])
            else:
                raise Exception('Could not guess macrocycle angles.')

        return mc_angle
