# -*- coding: utf-8 -*-
"""
Dataset class for Ifremer/IWWOC CFOSAT SWIM L2S files
"""
from pathlib import Path

from cerbere.dataset.dataset import Dataset


class IWWOCSWIML2SDataset(Dataset):
    pass


class IWWOCSWIML2SANAD1HzDataset(Dataset):
    """
    CWWIC L2 dataset as a nadir/trajectory product
    """
    def __init__(self, *args, **kwargs):

        field_matching = {
            'time': 'nadir_time_1Hz',
            'lon': 'nadir_lon_1Hz',
            'lat': 'nadir_lat_1Hz',
        }
        dim_matching = {
            'nadir_time_1Hz': 'time',
        }
        super(IWWOCSWIML2SANAD1HzDataset, self).__init__(
            *args,
            field_matching=field_matching,
            dim_matching=dim_matching,
            **kwargs
        )

        # remove other variables
        dims = set(self._std_dataset.dims) - {'nadir_time_1Hz'}
        self._std_dataset = self._std_dataset.drop_dims(dims).rename(
            {'nadir_time_1Hz': 'time',
             'nadir_lat_1Hz': 'lat',
             'nadir_lon_1Hz': 'lon'}).set_coords(['lat', 'lon'])

        return


    def _open(self, **kwargs):
        bundle_url = Path(self._url)

        self._url = bundle_url / (
                bundle_url.name.replace('L2S__', 'L2S10') + '.nc')
        self.dataset = super(IWWOCSWIML2SANAD1HzDataset, self)._open_dataset(
            *kwargs)

        self._url = bundle_url



