"""
Test class for cerbere CFOSAT L2 ANAD 1 Hz CWWIC dataset files

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import unittest

from tests.checker import Checker


class IWWOCSWIML2SANAD1HzDatasetChecker(Checker, unittest.TestCase):
    """Test class for CFOSAT L2 ANAD 1 Hz CWWIC dataset files"""

    def __init__(self, methodName="runTest"):
        super(IWWOCSWIML2SANAD1HzDatasetChecker, self).__init__(methodName)

    @classmethod
    def dataset(cls):
        """Return the mapper class name"""
        return 'IWWOCSWIML2SANAD1HzDataset'

    @classmethod
    def feature(cls):
        """Return the related feature class name"""
        return 'Trajectory'

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return "CFO_OP05_SWI_L2S____F_20200101T004055_20200101T021429_1.0.0"

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return "ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/"
