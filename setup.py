# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

long_desc = '''
This package contains the Cerbere extension for CFOSAT format.
'''

requires = ['cerbere>=2.0.0']

setup(
    name='cerberecontrib_cfosat',
    version='1.0',
    url='https://gitlab.ifremer.fr/cerbere/cerberecontrib-cfosat.git',
    license='GPLv3',
    authors='Antoine Grouazel, Gilles Guitton, Jean-François Piollé',
    author_email='jfpiolle@ifremer.fr',
    description='Cerbere extension for CFOSAT products',
    long_description=long_desc,
    zip_safe=False,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    platforms='any',
    packages=find_packages(),
    include_package_data=True,
    entry_points={
        'cerbere.plugins': [
            'CWWICSWIMNCDataset = cerberecontrib_cfosat.dataset.cwwicswimncdataset:CWWICSWIMNCDataset',
            'CWWICSWIML1ANCDataset = cerberecontrib_cfosat.dataset.cwwicswiml1ncdataset:CWWICSWIML1ANCDataset',
            'CWWICSWIML2NCDataset = cerberecontrib_cfosat.dataset.cwwicswiml2ncdataset:CWWICSWIML2NCDataset',
            'CWWICSWIML2BoxNCDataset = cerberecontrib_cfosat.dataset.cwwicswiml2ncdataset:CWWICSWIML2BoxNCDataset',
            'CWWICSWIML2NadirNCDataset = cerberecontrib_cfosat.dataset.cwwicswiml2ncdataset:CWWICSWIML2NadirNCDataset',
            'CWWICSWIML2ANADNCDataset = cerberecontrib_cfosat.dataset.cwwicswiml2ncdataset:CWWICSWIML2ANADNCDataset',
            'CWWICSWIML2ANAD1HzNCDataset = cerberecontrib_cfosat.dataset.cwwicswiml2ncdataset:CWWICSWIML2ANAD1HzNCDataset',
            'IWWOCSWIML2SANAD1HzDataset = '
            'cerberecontrib_cfosat.dataset.iwwocswiml2sdataset'
            ':IWWOCSWIML2SANAD1HzDataset',
            ]
    },
#    install_requires=requires,
)
